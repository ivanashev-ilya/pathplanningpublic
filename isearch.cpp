#include "isearch.h"

ISearch::ISearch()
{
    hweight = 1;
    breakingties = CN_SP_BT_GMAX;
}

ISearch::~ISearch(void) {}

int ISearch::convolution(int i, int j, const Map &map) {
    return i * map.getMapWidth() + j;
}

bool Node::breakingties;

SearchResult ISearch::startSearch(ILogger *Logger, const Map &map, const EnvironmentOptions &options)
{
    Node::breakingties = breakingties;
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    sresult.pathfound = false;
    sresult.numberofsteps = 0;
    Node cur(map.getStart_i(), map.getStart_j(), nullptr, 0,
             computeHFromCellToCell(map.getStart_i(), map.getStart_j(), map.getGoal_i(), map.getGoal_j(), options));
    sortByIndex[convolution(cur.i, cur.j, map)] = cur;
    open.insert(cur);
    while(!open.empty()) {
        ++sresult.numberofsteps;
        auto curIt = open.begin();
        cur = *open.begin();
        close[convolution(cur.i, cur.j, map)] = cur;
        Node *curPtr = &(close.find(convolution(cur.i, cur.j, map))->second);
        if (cur.i == map.getGoal_i() && cur.j == map.getGoal_j()) {
            sresult.pathfound = true;
            break;
        }
        std::list<Node> successors = findSuccessors(cur, map, options);
        for (auto neigh : successors) {
            if (close.find(convolution(neigh.i, neigh.j, map)) == close.end()) {
                neigh.parent = curPtr;
                resetParent(neigh, cur, map, options);
                auto it = sortByIndex.find(convolution(neigh.i, neigh.j, map));
                if (it == sortByIndex.end() || it->second.g > neigh.g) {
                    if (it != sortByIndex.end()) {
                        open.erase(open.find(it->second));
                    }
                    sortByIndex[convolution(neigh.i, neigh.j, map)] = neigh;
                    open.insert(neigh);
                }
            }
        }
        sortByIndex.erase(sortByIndex.find(convolution(cur.i, cur.j, map)));
        open.erase(curIt);
    }

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    int elapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    sresult.time = static_cast<double>(elapsedMilliseconds) / 1000;
    sresult.nodescreated = open.size() + close.size();
    if (sresult.pathfound) {
        auto it = close.find(convolution(map.getGoal_i(), map.getGoal_j(), map));
        sresult.pathlength = it->second.g;
        makePrimaryPath(it->second);
        makeSecondaryPath(map, options);
        sresult.hppath = &hppath; //Here is a constant pointer
        sresult.lppath = &lppath;
    }
    return sresult;
}

bool ISearch::checkCorners(int i1, int j1, int i2, int j2, const Map &map, const EnvironmentOptions &options) {
    return (options.cutcorners || map.CellIsTraversable(i1, j2) && map.CellIsTraversable(i2, j1)) &&
            (options.allowsqueeze || map.CellIsTraversable(i1, j2) || map.CellIsTraversable(i2, j1));
}

std::list<Node> ISearch::findSuccessors(const Node &curNode, const Map &map, const EnvironmentOptions &options)
{
    std::list<Node> successors;
    for (int di = -1; di <= 1; ++di) {
        for (int dj = -1; dj <= 1; ++dj) {
            int newi = curNode.i + di, newj = curNode.j + dj;
            if (map.CellOnGrid(newi, newj) && (di != 0 || dj != 0) && map.CellIsTraversable(newi, newj) &&
               (options.allowdiagonal || di == 0 || dj == 0) && checkCorners(newi, newj, curNode.i, curNode.j, map, options)) {
                Node neigh(newi, newj, nullptr, curNode.g,
                           computeHFromCellToCell(newi, newj, map.getGoal_i(), map.getGoal_j(), options));
                if (curNode.i == newi || curNode.j == newj) {
                    neigh.g += 1;
                }
                else {
                    neigh.g += CN_SQRT_TWO;
                }
                neigh.F = neigh.g + neigh.H;
                successors.push_back(neigh);
            }
        }
    }
    return successors;
}

void ISearch::resetParent(Node &current, const Node &parent, const Map &map, const EnvironmentOptions &options) {
    return;
}

void ISearch::makePrimaryPath(const Node &curNode)
{
    lppath.push_front(curNode);
    if (curNode.parent != nullptr) {
        makePrimaryPath(*(curNode.parent));
    }
}

void ISearch::makeSecondaryPath(const Map &map, const EnvironmentOptions &options)
{
    auto it = lppath.begin();
    hppath.push_back(*it);
    ++it;
    for (it; it != lppath.end(); ++it) {
        auto prevIt = it;
        --prevIt;
        auto nextIt = it;
        ++nextIt;
        if (nextIt == lppath.end() ||
            (it->i - prevIt->i) * (nextIt->j - it->j) != (it->j - prevIt->j) * (nextIt->i - it->i)) {
            hppath.push_back(*it);
        }
    }
}

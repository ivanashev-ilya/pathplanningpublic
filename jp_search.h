#ifndef JP_SEARCH_H
#define JP_SEARCH_H
#include "astar.h"

class JP_Search:public Astar
{
public:
    JP_Search(float hweight, bool breakingties):Astar(hweight, breakingties){}
    ~JP_Search();

private:
    std::list<Node> findSuccessors(const Node &curNode, const Map &map, const EnvironmentOptions &options) override;
    void makePrimaryPath(const Node &curNode) override;
    void makeSecondaryPath(const Map &map, const EnvironmentOptions &options) override;
    bool getPrunedNeighbours(Node curNode, int di, int dj, std::list<Node>& neighbours, const Map &map, bool onlyCheck);
    Node jump(const Node &curNode, int di, int dj, const Map &map);
    int sign(int x);
};

#endif // JP_SEARCH_H

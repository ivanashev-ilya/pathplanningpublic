#ifndef THETA_H
#define THETA_H
#include "astar.h"

class Theta: public Astar
{
    public:
        Theta(double hweight, bool breakingties):Astar(hweight, breakingties){}
        ~Theta(void);

    private:
        bool lineOfSight(int i1, int j1, int i2, int j2, const Map &map, const EnvironmentOptions &options);
        void makePrimaryPath(const Node &curNode) override;
        void makeSecondaryPath(const Map &map, const EnvironmentOptions &options) override;
        bool bresenhamLineAlgorithm(int i1, int j1, int i2, int j2, std::list<Node>& path,
                                    const EnvironmentOptions &options, const Map &map = {});
        void resetParent(Node &current, const Node &parent, const Map &map, const EnvironmentOptions &options) override;
};


#endif // THETA_H

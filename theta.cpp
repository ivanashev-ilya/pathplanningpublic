#include "theta.h"
Theta::~Theta()
{
}

bool Theta::bresenhamLineAlgorithm(int i1, int j1, int i2, int j2, std::list<Node>& path, const EnvironmentOptions &options, const Map &map) {
    bool reflected = false, reversed = false;
    if (abs(i1 - i2) < abs(j1 - j2)) {
        std::swap(i1, j1);
        std::swap(i2, j2);
        reflected = true;
    }
    if (i1 > i2) {
        std::swap(i1, i2);
        std::swap(j1, j2);
        reversed = true;
    }

    int deltai = abs(i1 - i2), deltaj = abs(j1 - j2);
    int error = 0;
    int curj = j1, dirj;
    if (j1 < j2) {
        dirj = 1;
    } else if (j1 > j2) {
        dirj = -1;
    } else {
        dirj = 0;
    }

    int previ = -1, prevj = -1;
    for (int curi = i1; curi <= i2; ++curi) {
        Node curNode(curi, curj);
        if (reflected) {
            std::swap(curNode.i, curNode.j);
        }
        if (map.CellIsObstacle(curNode.i, curNode.j) ||
           (previ != -1 && !checkCorners(curNode.i, curNode.j, previ, prevj, map, options))) {
            return false;
        }
        if (reversed) {
            path.push_front(curNode);
        } else {
            path.push_back(curNode);
        }
        previ = curNode.i;
        prevj = curNode.j;

        error += deltaj;
        if (2 * error >= deltai) {
            curj += dirj;
            error -= deltai;
        }
    }
    return true;
}

bool Theta::lineOfSight(int i1, int j1, int i2, int j2, const Map &map, const EnvironmentOptions &options)
{
    std::list<Node> path;
    return bresenhamLineAlgorithm(i1, j1, i2, j2, path, options, map);
}

void Theta::resetParent(Node &current, const Node &parent, const Map &map, const EnvironmentOptions &options) {
    Node* ancestor = parent.parent;
    if (ancestor != nullptr && lineOfSight(current.i, current.j, ancestor->i, ancestor->j, map, options)) {
        double dist = euclideanDistance(current.i, current.j, ancestor->i, ancestor->j);
        current.g = ancestor->g + dist;
        current.F = current.g + current.H;
        current.parent = ancestor;
    }
}

void Theta::makeSecondaryPath(const Map &map, const EnvironmentOptions &options)
{
    auto it = hppath.begin();
    lppath.push_back(*it);
    ++it;
    for (it; it != hppath.end(); ++it) {
        auto prevIt = it;
        --prevIt;
        lppath.pop_back();
        std::list<Node> path;
        bresenhamLineAlgorithm(prevIt->i, prevIt->j, it->i, it->j, path, options, map);
        lppath.insert(lppath.end(), path.begin(), path.end());
    }
}

void Theta::makePrimaryPath(const Node &curNode)
{
    hppath.push_front(curNode);
    if (curNode.parent != nullptr) {
        makePrimaryPath(*(curNode.parent));
    }
}

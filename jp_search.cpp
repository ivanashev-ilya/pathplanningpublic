#include "jp_search.h"

JP_Search::~JP_Search()
{
}

int JP_Search::sign(int x) {
    if (x) {
        return x / std::abs(x);
    }
    return 0;
}

//Function returns true, if there are forced neighbours
bool JP_Search::getPrunedNeighbours(Node curNode, int di, int dj, std::list<Node>& neighbours, const Map &map, bool onlyCheck = false) {
    Node prevNode(curNode.i - di, curNode.j - dj);
    bool reversed = false;
    bool forcedFound = false;
    if (curNode.j == prevNode.j){
        std::swap(curNode.i, curNode.j);
        std::swap(prevNode.i, prevNode.j);
        std::swap(di, dj);
        reversed = true;
    }
    if (curNode.i == prevNode.i) {
        int newj = curNode.j + dj;
        for (int newi = curNode.i - 1; newi <= curNode.i + 1; ++newi) {
            int reali = newi, realj = newj;
            if (reversed) {
                std::swap(reali, realj);
            }
            if (map.CellOnGrid(reali, realj) && map.CellIsTraversable(reali, realj) && (newi == curNode.i
                 || (!reversed ? map.CellIsObstacle(newi, curNode.j) : map.CellIsObstacle(curNode.j, realj)))) {
                neighbours.push_back(Node(reali, realj));
                if (newi != curNode.i) {
                    forcedFound = true;
                    if (onlyCheck) {
                        return forcedFound;
                    }
                }
            }
        }
    } else {
        int newi = curNode.i + di, newj;
        for (newj = prevNode.j; abs(newj - prevNode.j) <= 2; newj += dj) {
            if (map.CellOnGrid(newi, newj) && map.CellIsTraversable(newi, newj) &&
                (newj != prevNode.j || map.CellIsObstacle(curNode.i, newj))) {
                neighbours.push_back(Node(newi, newj));
                if (newj == prevNode.j) {
                    forcedFound = true;
                    if (onlyCheck) {
                        return forcedFound;
                    }
                }
            }
        }
        newj = curNode.j + dj;
        for (newi = prevNode.i; abs(newi - prevNode.i) <= 1; newi += di) {
            if (map.CellOnGrid(newi, newj) && map.CellIsTraversable(newi, newj) &&
                (newi != prevNode.i || map.CellIsObstacle(newi, curNode.j))) {
                neighbours.push_back(Node(newi, newj));
                if (newi == prevNode.i) {
                    forcedFound = true;
                    if (onlyCheck) {
                        return forcedFound;
                    }
                }
            }
        }
    }
    return forcedFound;
}

std::list<Node> JP_Search::findSuccessors(const Node &curNode, const Map &map, const EnvironmentOptions &options)
{
    std::list<Node> successors, neighbours;
    if (curNode.parent == nullptr) {
        for (int di = -1; di <= 1; ++di) {
            for (int dj = -1; dj <= 1; ++dj) {
                int newi = curNode.i + di, newj = curNode.j + dj;
                if (map.CellOnGrid(newi, newj) && (di != 0 || dj != 0) && map.CellIsTraversable(newi, newj)) {
                    neighbours.push_back(Node(newi, newj));
                }
            }
        }
    } else {
        getPrunedNeighbours(curNode, sign(curNode.i - curNode.parent->i), sign(curNode.j - curNode.parent->j), neighbours, map);
    }
    for (Node neigh : neighbours) {
        neigh = jump(curNode, neigh.i - curNode.i, neigh.j - curNode.j, map);
        if (neigh.i != -1) {
            neigh.g = curNode.g + metric(curNode.i, curNode.j, neigh.i, neigh.j, options.metrictype);
            neigh.H = computeHFromCellToCell(neigh.i, neigh.j, map.getGoal_i(), map.getGoal_j(), options);
            neigh.F = neigh.g + neigh.H;
            successors.push_back(neigh);
        }
    }
    return successors;
}

Node JP_Search::jump(const Node &curNode, int di, int dj, const Map &map) {
    std::list<Node> neighbours;
    Node neigh = Node(curNode.i + di, curNode.j + dj);
    if (!map.CellOnGrid(neigh.i, neigh.j) || map.CellIsObstacle(neigh.i, neigh.j)) {
        return Node(-1);
    }
    if (neigh.i == map.getGoal_i() && neigh.j == map.getGoal_j() ||
            getPrunedNeighbours(neigh, di, dj, neighbours, map, true)) {
        return neigh;
    }
    if (di != 0 && dj != 0) {
        Node jumpi = jump(neigh, di, 0, map);
        if (jumpi.i != -1) {
            return neigh;
        }
        Node jumpj = jump(neigh, 0, dj, map);
        if (jumpj.i != -1) {
            return neigh;
        }
    }
    return jump(neigh, di, dj, map);
}

void JP_Search::makePrimaryPath(const Node &curNode)
{
    hppath.push_front(curNode);
    if (curNode.parent != nullptr) {
        makePrimaryPath(*(curNode.parent));
    }
}

void JP_Search::makeSecondaryPath(const Map &map, const EnvironmentOptions &options)
{
    auto it = hppath.begin();
    lppath.push_back(*it);
    ++it;
    for (it; it != hppath.end(); ++it) {
        auto prevIt = it;
        --prevIt;
        lppath.pop_back();
        int di = sign(it->i - prevIt->i), dj = sign(it->j - prevIt->j);
        int i, j;
        for (i = prevIt->i, j = prevIt->j; i != it->i || j != it->j; i += di, j += dj) {
            lppath.push_back(Node(i, j));
        }
        lppath.push_back(Node(i, j));
    }
}

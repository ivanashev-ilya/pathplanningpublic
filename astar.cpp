#include "astar.h"

Astar::Astar(double HW, bool BT)
{
    hweight = HW;
    breakingties = BT;
}

double Astar::computeHFromCellToCell(int i1, int j1, int i2, int j2, const EnvironmentOptions &options)
{
    return metric(i1, j1, i2, j2, options.metrictype) * hweight;
}

double Astar::euclideanDistance(int x1, int y1, int x2, int y2) {
    double res = sqrt(double((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    return res;
}

double Astar::manhattanDistance(int x1, int y1, int x2, int y2) {
    return abs(x1 - x2) + abs(y1 - y2);
}

double Astar::chebyshevDistance(int x1, int y1, int x2, int y2) {
    return std::max(abs(x1 - x2), abs(y1 - y2));
}

double Astar::diagonalDistance(int x1, int y1, int x2, int y2) {
    return (sqrt(2.0) - 1) * std::min(abs(x1 - x2), abs(y1 - y2)) + std::max(abs(x1 - x2), abs(y1 - y2));
}

double Astar::metric(int x1, int y1, int x2, int y2, int metrictype) {
    if (metrictype == CN_SP_MT_DIAG) {
        return diagonalDistance(x1, y1, x2, y2);
    } else if (metrictype == CN_SP_MT_CHEB) {
        return chebyshevDistance(x1, y1, x2, y2);
    } else if (metrictype == CN_SP_MT_EUCL) {
        return euclideanDistance(x1, y1, x2, y2);
    } else if (metrictype == CN_SP_MT_MANH) {
        return manhattanDistance(x1, y1, x2, y2);
    }
}
